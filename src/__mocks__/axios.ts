import data from './rates'

module.exports = {
  get: () => {
    return Promise.resolve(data)
  },
}
