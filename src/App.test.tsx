import React from 'react'
import {
  cleanup,
  fireEvent,
  queryByTestId,
  render,
  RenderResult,
} from '@testing-library/react'
import App from './App'
import { Store } from 'redux'
import { Provider } from 'react-redux'
import configureStore from './store/configureStore'
import axios from 'axios'

jest.mock('axios')

const renderComponent = (store: Store): RenderResult => {
  const mainComponent = render(
    <Provider store={store}>
      <App />
    </Provider>
  )

  return mainComponent
}

describe('App component', () => {
  it('calls api when select change', async () => {
    const store = configureStore()
    const mainContainerComponent = renderComponent(store)
    const getSpy = jest.spyOn(axios, 'get')
    const selectFromSelector = (await mainContainerComponent.findByTestId(
      'fromCurrencySelect'
    )) as HTMLSelectElement
    fireEvent.change(selectFromSelector, { target: { value: 'GBP' } })
    expect(getSpy).toBeCalled()
    expect(selectFromSelector.options[2].selected).toBeTruthy()
    cleanup()
  })
})
