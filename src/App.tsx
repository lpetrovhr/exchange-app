import React, { useEffect, useState } from 'react'
import './app.scss'
import { useDispatch, useSelector } from 'react-redux'
import { changeBaseCurrency, fetchDataRequest } from './store/actions/actions'
import { ApplicationState } from './store/reducers'
import { calculateRate } from './helpers/rateCalculator'
import CurrencySelect from './components/currency-selector/CurrencySelect'
import NumberFormat from 'react-number-format'

function App() {
  const dispatch = useDispatch()
  const selectedBaseCurrency = useSelector(
    (s: ApplicationState) => s.user.selectedCurrency
  )
  const rates = useSelector((s: ApplicationState) => s.rates)
  const [baseCurrency, setBaseCurrency] = useState('EUR')
  const [convertFrom, setConvertFrom] = useState('')
  const [convertTo, setConvertTo] = useState('')
  const [convertToCurrency, setConvertToCurrency] = useState('USD')

  useEffect(() => {
    dispatch(fetchDataRequest(baseCurrency))
  }, [])

  useEffect(() => {
    const getRates = () => {
      dispatch(fetchDataRequest(baseCurrency))
    }

    getRates()
    const interval = setInterval(() => getRates(), 10000)
    return () => {
      clearInterval(interval)
    }
  }, [baseCurrency])

  const changeConvertFrom = (event: React.FormEvent<HTMLSelectElement>) => {
    setBaseCurrency(event.currentTarget.value)
    dispatch(fetchDataRequest(event.currentTarget.value))
  }

  const changeConvertTo = (event: React.FormEvent<HTMLSelectElement>) => {
    setConvertToCurrency(event.currentTarget.value)
  }

  const onConvertFromInputChange = (
    event: React.FormEvent<HTMLInputElement>
  ) => {
    const inputValue = event.currentTarget.value
    setConvertFrom(inputValue)
    setConvertTo(
      calculateRate(
        rates,
        convertToCurrency,
        parseFloat(inputValue),
        true
      ).toString()
    )
  }

  const onConvertToInputChange = (event: React.FormEvent<HTMLInputElement>) => {
    const inputValue = event.currentTarget.value
    setConvertTo(inputValue)
    setConvertFrom(
      calculateRate(
        rates,
        convertToCurrency,
        parseFloat(inputValue),
        false
      ).toString()
    )
  }

  return (
    <>
      <main className="main-container">
        <section className="convert-from-currency">
          <CurrencySelect
            defaultValue={selectedBaseCurrency}
            onChangeCallback={changeConvertFrom}
            testId="fromCurrencySelect"
          />
          <NumberFormat
            placeholder="0.00"
            decimalScale={2}
            onChange={onConvertFromInputChange}
            value={convertFrom}
          />
        </section>
        <section className="convert-to-currency">
          <CurrencySelect
            defaultValue={convertToCurrency}
            onChangeCallback={changeConvertTo}
            testId="toCurrencySelect"
          />
          <NumberFormat
            placeholder="0.00"
            decimalScale={2}
            onChange={onConvertToInputChange}
            value={convertTo}
          />
        </section>
      </main>
    </>
  )
}

export default App
