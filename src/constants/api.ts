export const EXCHANGE_API_BASE = 'https://api.exchangeratesapi.io'

export const LATEST_RATES_URL = `${EXCHANGE_API_BASE}/latest`
