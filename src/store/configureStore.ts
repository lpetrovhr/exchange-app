import { applyMiddleware, compose, createStore, StoreEnhancer } from 'redux'
import thunk from 'redux-thunk'
import rootReducer from './reducers'

const configureStore = () => {
  /**
   * Redux Middleware. We split dev & prod environments to exclude dev tooling from prod bundle
   */
  const middleware = ((): StoreEnhancer => {
    let _middleware: StoreEnhancer

    if (
      process.env.NODE_ENV !== 'production' &&
      process.env.NODE_ENV !== 'test'
    ) {
      // Use inline `require` instead of top `import` to get dead-code-elimination working and exclude dev tooling from prod bundle
      // eslint-disable-next-line @typescript-eslint/no-var-requires
      const { composeWithDevTools } = require('redux-devtools-extension')
      // eslint-disable-next-line @typescript-eslint/no-var-requires

      _middleware = composeWithDevTools(applyMiddleware(thunk))
    } else {
      _middleware = compose(applyMiddleware(thunk))
    }

    return _middleware
  })()

  return createStore(rootReducer, middleware)
}

export type ApplicationStore = ReturnType<typeof configureStore>

export default configureStore
