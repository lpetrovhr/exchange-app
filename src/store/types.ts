export enum RatesActionType {
  FETCH_RATES_DATA = 'FETCH_RATES_DATA',
  FETCH_RATES_SUCCESS = 'FETCH_RATES_SUCCESS',
}

export enum UserActionType {
  CHANGE_BASE_CURRENCY = 'CHANGE_BASE_CURRENCY',
}

export interface UserState {
  selectedCurrency: string
}

export interface RatesState {
  data: {
    base: string
    date: string
    rates: { [key: string]: number }
  } | null
}
