import { Reducer } from 'redux'
import { RatesActionType, RatesState } from '../types'

export const initialState = (): RatesState => ({
  data: null,
})

const ratesReducer: Reducer<RatesState> = (state = initialState(), action) => {
  switch (action.type) {
    case RatesActionType.FETCH_RATES_SUCCESS: {
      return {
        ...state,
        data: action.data,
      }
    }
    default:
      return state
  }
}

export { ratesReducer as RatesReducer }
