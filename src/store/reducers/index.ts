import { combineReducers } from 'redux'
import { UserReducer } from './user'
import { RatesReducer } from './rates'

const rootReducer = combineReducers({
  rates: RatesReducer,
  user: UserReducer,
})

export type ApplicationState = ReturnType<typeof rootReducer>

export default rootReducer
