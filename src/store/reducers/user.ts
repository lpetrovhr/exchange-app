import { Reducer } from 'redux'
import { UserActionType, UserState } from '../types'

export const initialState = (): UserState => ({
  selectedCurrency: 'EUR',
})

const userReducer: Reducer<UserState> = (state = initialState(), action) => {
  switch (action.type) {
    case UserActionType.CHANGE_BASE_CURRENCY: {
      return {
        ...state,
        selectedCurrency: action.currency,
      }
    }
    default:
      return state
  }
}

export { userReducer as UserReducer }
