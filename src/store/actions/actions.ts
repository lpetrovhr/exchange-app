import { ActionCreator, Action, Dispatch } from 'redux'
import { ThunkAction } from 'redux-thunk'
import axios from 'axios'

import { RatesActionType, UserActionType } from '../../store/types'

import { ApplicationState } from '../reducers'
import { LATEST_RATES_URL } from '../../constants/api'

export type AppThunk = ActionCreator<
  ThunkAction<void, ApplicationState, null, Action<string>>
>

export const fetchDataSuccess = (data: Object) => ({
  type: RatesActionType.FETCH_RATES_SUCCESS,
  data,
})

export const fetchDataRequest: AppThunk = (selectedCurrency: string) => (
  dispatch: Dispatch
) => {
  axios
    .get(`${LATEST_RATES_URL}?base=${selectedCurrency}`)
    .then((res) => res.data)
    .then((data) => {
      dispatch(fetchDataSuccess(data))
    })
    .catch(() => console.error('error fetching api'))
}

export const changeBaseCurrency = (currency: string) => ({
  type: UserActionType.CHANGE_BASE_CURRENCY,
  currency,
})
