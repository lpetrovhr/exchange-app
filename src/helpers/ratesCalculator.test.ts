import { calculateRate } from './rateCalculator'
import mockData from '../__mocks__/rates'

describe('Rates calculator', () => {
  it('calculates rate on given data', async () => {
    expect(calculateRate(mockData, 'GBP', 2, true)).toEqual(1.8086)
  })
})
