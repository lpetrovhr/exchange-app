// rate calculator helper
import { RatesState } from '../store/types'

export const calculateRate = (
  rates: RatesState,
  convertToCurrency: string,
  convertValue: number,
  convertToBoolean: boolean
) => {
  const conversionRate = rates.data?.rates[convertToCurrency] || 1
  return convertToBoolean
    ? conversionRate * convertValue
    : convertValue / conversionRate
}
