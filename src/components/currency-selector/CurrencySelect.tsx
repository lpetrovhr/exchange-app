import React from 'react'
import './currencySelect.scss'

type CurrencySelectProps = {
  defaultValue: string
  onChangeCallback: (event: React.FormEvent<HTMLSelectElement>) => void
  testId: string
}

const CurrencySelect = ({
  defaultValue,
  onChangeCallback,
  testId,
}: CurrencySelectProps) => {
  return (
    <div className="select-container">
      <select
        className="currency-select"
        defaultValue={defaultValue}
        onChange={onChangeCallback}
        data-testid={testId}
      >
        <option data-testid="select-option" value="EUR">
          EUR
        </option>
        <option data-testid="select-option" value="USD">
          USD
        </option>
        <option data-testid="select-option" value="GBP">
          GBP
        </option>
      </select>
    </div>
  )
}

export default CurrencySelect
