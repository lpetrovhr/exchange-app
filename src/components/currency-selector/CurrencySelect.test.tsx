import React from 'react'
import renderer from 'react-test-renderer'
import CurrencySelect from './CurrencySelect'

describe('CurrencySelect component', () => {
  test('snapshot renders', () => {
    const component = renderer.create(
      <CurrencySelect
        onChangeCallback={jest.fn()}
        defaultValue={'Test'}
        testId={'Test'}
      />
    )

    const tree = component.toJSON()
    expect(tree).toMatchInlineSnapshot(`
      <div
        className="select-container"
      >
        <select
          className="currency-select"
          data-testid="Test"
          defaultValue="Test"
          onChange={[MockFunction]}
        >
          <option
            data-testid="select-option"
            value="EUR"
          >
            EUR
          </option>
          <option
            data-testid="select-option"
            value="USD"
          >
            USD
          </option>
          <option
            data-testid="select-option"
            value="GBP"
          >
            GBP
          </option>
        </select>
      </div>
    `)
  })
})
